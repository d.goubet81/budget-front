import { Entry } from "../entities/Entry";
import { lstCategories, lstEntries } from "../entities/listes";
import { refreshBudget } from "../main";
import { setEntries } from "../utils/storage";

let today = new Date().toISOString().slice(0, 10);
const formChecked = 'was-validated';

const btnAdd = document.querySelector('#btn-add');

const popup = document.querySelector<HTMLElement>('#popup');

const popupForm = document.querySelector<HTMLFormElement>('#popup-form');
const popupDate = document.querySelector<HTMLInputElement>('#popup-date');
const popupCat = document.querySelector<HTMLSelectElement>('#popup-cat');
const popupLabel = document.querySelector<HTMLInputElement>('#popup-label');
const popupDebit = document.querySelector<HTMLInputElement>('#popup-debit');
const popupCredit = document.querySelector<HTMLInputElement>('#popup-credit');
const popupSign = document.querySelector<HTMLInputElement>('#popup-sign');
const popupAmount = document.querySelector<HTMLInputElement>('#popup-amount');
const popupCancel = document.querySelector<HTMLInputElement>('#popup-cancel');


// Event Add Entry
btnAdd?.addEventListener('click', () => {
  openForm();
});

// Event Radio Buttons Change
popupDebit!.addEventListener('change', () => {
  popupSign!.textContent = popupDebit!.value;
});
popupCredit!.addEventListener('change', () => {
  popupSign!.textContent = popupCredit!.value;
});

// Event Form submit
popupForm!.addEventListener('submit', (event) => {
  event.preventDefault();

  // Bootstrap validation display
  popupForm!.classList.add(formChecked);

  // Check if for is valid
  if (popupForm?.checkValidity()) {
    let newEntry = new Entry(popupDate?.valueAsDate!,
      lstCategories[popupCat?.selectedIndex!],
      popupLabel?.value!,
      Number(popupSign?.textContent! + popupAmount?.valueAsNumber!));
    insertEntry(newEntry, lstEntries);
    closeForm();
    refreshBudget();
  }
}, false);

// Event Cancel Button
popupCancel?.addEventListener('click', () => {
  closeForm();
});

export function popupInitialization() {
  initDate();
  initCategories();
}

function initDate() {
  popupDate!.max = today;
}

function initCategories() {
  for (let idx = 1; idx < lstCategories.length; idx++) {
    let option = document.createElement('option');
    option.value = lstCategories[idx].name;
    option.textContent = lstCategories[idx].name;
    popupCat?.append(option);
  }
}

function insertEntry(entry: Entry, list: Entry[]) {
  let inserted = false;
  let idx = 0;
  while (!inserted) {
    if (idx < list.length) {
      if (entry.date > list[idx].date) {
        list.splice(idx, 0, entry);
        inserted = true;
      }
    } else {
      list.push(entry);
      inserted = true;
    }
    idx++;
  }
  if(inserted){
    setEntries(lstEntries);
  }
}

function openForm() {
  popupForm?.reset();
  popupDate!.value = today;
  popup!.style.display = 'block';
}

function closeForm() {
  popupForm!.classList.remove(formChecked);
  popupForm?.reset();
  popup!.style.display = 'none';
}