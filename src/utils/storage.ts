import { Category } from "../entities/Category";
import { Entry } from "../entities/Entry";

let lsCategories='categories';
let lsEntries='entries';

export function getCategories(): Category[] {
  let list: Category[] = [];
  let jsonCat = localStorage.getItem(lsCategories);
  if (jsonCat) {
    list = JSON.parse(jsonCat);
  }
  return list;
}

export function setCategories(list: Category[]) {
  localStorage.setItem(lsCategories, JSON.stringify(list));
}

export function getEntries(): Entry[] {
  let list: Entry[] = [];
  let jsonEntries = localStorage.getItem(lsEntries);
  if (jsonEntries) {
    list = JSON.parse(jsonEntries).map((item: Entry) => {
      item.date = new Date(item.date);
      return item;
    });
  }
  return list;
}

export function setEntries(list: Entry[]) {
  localStorage.setItem(lsEntries, JSON.stringify(list));
}