import { Entry } from '../entities/Entry';
import { lstEntries } from '../entities/listes';
import { refreshBudget } from '../main';
import { convertDate } from '../utils/date';
import { justifyCenter, justifyRigth } from '../utils/justification';
import { setEntries } from '../utils/storage';

export function createIcon(icon: string): HTMLTableCellElement {
  let td = document.createElement('td');
  td.className = 'd-block d-md-none';
  if (icon != '') {
    let img = document.createElement('img');
    img.src = `../icons/${icon}.png`;
    img.setAttribute('height', '15px');
    img.setAttribute('width', '15px');
    td.append(img);
  }
  return td;
}

export function createDate(date: Date): HTMLTableCellElement {
  let td = document.createElement('td');
  td.append(justifyCenter(convertDate(date)));
  return td;
}

export function createCategory(cat: string): HTMLTableCellElement {
  let td = document.createElement('td');
  td.className = 'd-none d-md-block';
  td.append(justifyCenter(cat));
  return td;
}

export function createLabel(label: string): HTMLTableCellElement {
  let td = document.createElement('td');
  td.append(justifyCenter(label));

  return td;
}

export function createAmount(value: number): HTMLTableCellElement {
  let td = document.createElement('td');

  if (value < 0) {
    td.classList.add("debit");
  } else {
    td.classList.add("credit");
  }

  td.append(justifyRigth(value.toFixed(2) + ' €'));

  return td;
}

export function createRemove(n: number): HTMLTableCellElement {
  let td = document.createElement('td');

  let divCont = document.createElement('div');
  divCont.className = 'container';

  let divRow = document.createElement('div');
  divRow.className = 'row justify-content-center';

  let b = document.createElement('button');
  b.textContent = 'x';
  b.className = 'rem-btn btn btn-outline-danger btn-sm p-0';
  b.addEventListener('click', () => {
    lstEntries.splice(n, 1);
    setEntries(lstEntries);
    refreshBudget();
  });

  divRow.append(b);
  divCont.append(divRow);
  td.append(divCont);

  return td;
}

export function builtLstEntries(entries: Entry[]): HTMLElement {
  let tbody = document.createElement('tbody')
  tbody.id = 'lst-entries';
  // tbody.className = 'table-light';

  for (let idx = 0; idx < entries.length; idx++) {
    tbody.append(builtEntry(entries[idx], idx));
  }
  return tbody;
}

function builtEntry(entry: Entry, idx: number): HTMLElement {
  let tr = document.createElement('tr');
  tr.id = `entry-${idx}`;
  //Icon
  tr.append(createIcon(entry.category.icon));
  // Date
  tr.append(createDate(entry.date));
  // Category
  tr.append(createCategory(entry.category.name));
  // Label
  tr.append(createLabel(entry.label));
  // Amount
  tr.append(createAmount(entry.amount));
  //Remove button
  tr.append(createRemove(idx));

  return tr;
}