export function justifyLeft(text:string):HTMLDivElement{
  let divCont=document.createElement('div');
  divCont.className='container';

  let divRow=document.createElement('div');
  divRow.className='row justify-content-start ps-lg-3 pe-lg-3';
  divRow.textContent=text;

  divCont.append(divRow);

  return divCont;
}

export function justifyCenter(text:string):HTMLDivElement{
  let divCont=document.createElement('div');
  divCont.className='container';

  let divRow=document.createElement('div');
  divRow.className='row justify-content-center ps-lg-3 pe-lg-3';
  divRow.textContent=text;

  divCont.append(divRow);

  return divCont;
}

export function justifyRigth(text:string):HTMLDivElement{
  let divCont=document.createElement('div');
  divCont.className='container';

  let divRow=document.createElement('div');
  divRow.className='row justify-content-end ps-lg-3 pe-lg-3';
  divRow.textContent=text;

  divCont.append(divRow);

  return divCont;
}
