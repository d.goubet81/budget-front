import { justifyCenter, justifyRigth } from "../utils/justification";

export function adjustHeader() {

  // Date
  const thDate = document.querySelector('#h-date');
  let hDate = thDate?.textContent;
  thDate!.textContent = '';
  thDate!.append(justifyCenter(hDate!));

  // Category
  const thCat = document.querySelector('#h-cat');
  let hCat = thCat?.textContent;
  thCat!.textContent = '';
  thCat!.append(justifyCenter(hCat!));

  // Label
  const thLabel = document.querySelector('#h-label');
  let hLabel = thLabel?.textContent;
  thLabel!.textContent = '';
  thLabel!.append(justifyCenter(hLabel!));

  // Amount
  const thAmount = document.querySelector('#h-amount');
  let hAmount = thAmount?.textContent;
  thAmount!.textContent = '';
  thAmount!.append(justifyRigth(hAmount!));
}