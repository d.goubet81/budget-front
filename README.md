# Budget-front

L'objectif du projet est de créer une application de gestion de budget.

## Présentation
* Rendu
* Structure
* Améliorations

### Rendu
***
[Lien vers la maquette.](https://whimsical.com/budget-front-MEqwyiPv53uyt6PcVses3R)

#### Application Ecran large
La catégorie est affiché en texte.

<img src="img/Web.png" alt="" width="500">

#### Application Ecran réduit
La catégorie est affiché au moyen d'une icone.

<img src="img/Mobile.png" alt="" width="300">

### Structure
***
Le *main.ts* déclenche l'affichage des données du tableau.

#### Entities
Regroupe la déclaration des entités et leur initialisation :
* **Category** : déclaration de la classe Category avec un nom et une icone.
* **Entry** : déclaration de la classe Entry contenant la structure d'une entrée dans le budget.
* **listes** : déclaration et initialisation des listes catégorie et entrées.

#### table
Regroupe les éléments permettant de gérer l'affichage du budget :
* **theader** : gère l'affiche du header du tableau.
* **tbody** : gère l'affiche du bobyè du tableau.
* **tfooter** : gère l'affiche du footer du tableau.

#### extra
Regroupe la gestion des éléments annexes au tableau budget :
* **filter** : génération du select mois et de la liste des entrées filtrées, fonctionnelle mais pas implantée.
* **popup** : gestion du popup pour la saisie d'une nouvelle entrée.

#### utils
Regroupe les utilitaires :
* **date** : tout ce qui a attrait au date (formatage, liste des mois...).
* **justification** : routines permettant d'aligner un texte (gauche, droite, centre)
* **storage** : methodes pour le local storage (set et get).

### Améliorations
***
Plusieurs pistes d'amélioration possible.
* adaptation de l'affichage des entrées du budget avec la liste des entrées filtrées et la gestion de la suppression d'un élément depuis cette affichage.
* ajout d'une fenêtre de confirmation avant suppression.
* possibilité d'éditer une entrée.
* possibilité de rajouter des catégories.
* ajout d'un affichage graphique.

