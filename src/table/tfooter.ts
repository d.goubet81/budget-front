import { lstEntries } from "../entities/listes";
import { justifyRigth } from "../utils/justification";

export function displayBalance() {
  const balance = document.querySelector<HTMLElement>('#balance');
  balance!.textContent = '';
  balance?.append(justifyRigth(calculateBalance().toFixed(2) + ' €'));
}

function calculateBalance(): number {
  let balance = 0;
  for (const entry of lstEntries) {
    balance += entry.amount;
  }
  return balance;
}