import { getCategories, getEntries, setCategories, setEntries } from "../utils/storage";
import { Category } from "./Category";
import { Entry } from "./Entry";

export let lstCategories: Category[] = [];

lstCategories.push(new Category('', ''));
lstCategories.push(new Category('Paie', 'coin'));
lstCategories.push(new Category('Loyer', 'house'));
lstCategories.push(new Category('Loisir', 'masks'));
lstCategories.push(new Category('Alimentation', 'fork'));
// setCategories(lstCategories);

// lstCategories = getCategories();


export let lstEntries: Entry[] = [];

// lstEntries.push(new Entry(new Date('2022-04-08'), lstCategories[2], 'Loyer d\'Avril', -463.47));
// lstEntries.push(new Entry(new Date('2022-04-03'), lstCategories[1], 'Paie de Mars', 1542.69));
// lstEntries.push(new Entry(new Date('2022-04-02'), lstCategories[4], 'Course alimentaire', -86.33));
// lstEntries.push(new Entry(new Date('2022-04-01'), lstCategories[3], 'Restaurant', -62.68));
// lstEntries.push(new Entry(new Date('2022-03-24'), lstCategories[3], 'Concert', -53.14));
// lstEntries.push(new Entry(new Date('2022-03-18'), lstCategories[3], 'Spectacle', -80));
// lstEntries.push(new Entry(new Date('2022-03-17'), lstCategories[4], 'Restaurant', -22.9));
// lstEntries.push(new Entry(new Date('2022-03-15'), lstCategories[3], 'Cinéma', -13.44));
// setEntries(lstEntries);

lstEntries = getEntries();