import { lstEntries } from "../entities/listes";
import { getMonthList } from "../utils/date";

let selFilter = document.querySelector<HTMLSelectElement>('#filter');


initFilter(getMonthList('fr'), selFilter!);

selFilter?.addEventListener('change', () => {
  const fltMonth = selFilter!.selectedIndex - 1;
  let lstFltEntries = [];
  if (fltMonth >= 0) {
    lstFltEntries = lstEntries.filter(entry =>
      entry.date.getMonth() == fltMonth
    );
  } else {
    lstFltEntries = lstEntries
  }

  console.log(lstFltEntries);
});

function initFilter(list: string[], select: HTMLSelectElement) {
  for (const element of list) {
    let option = document.createElement('option');
    option.value = element;
    option.textContent = element[0].toUpperCase() + element.slice(1);
    select?.append(option);
  }
}