import { Category } from "./Category";

export class Entry {
  date: Date;
  category: Category;
  label: string;
  amount: number;

  constructor(date: Date, category: Category, label: string, amount: number) {
    this.date = date;
    this.category = category;
    this.label = label;
    this.amount = amount;
  }
}