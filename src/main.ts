import './style.css';
import './extra/filter'
import { lstEntries } from "./entities/listes";

import { adjustHeader } from './table/theader';
import { builtLstEntries } from './table/tbody';
import { displayBalance } from './table/tfooter';
import { popupInitialization } from './extra/popup';

adjustHeader();
refreshBudget();
popupInitialization();

export function refreshBudget() {
  displayEntries(builtLstEntries(lstEntries));
  displayBalance();
}

export function displayEntries(e: HTMLElement) {
  const table = document.querySelector<HTMLTableElement>('table');
  const entries = document.querySelector<HTMLElement>('#lst-entries');
  table?.replaceChild(e, entries!);
}